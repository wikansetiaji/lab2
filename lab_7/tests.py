from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_friend, validate_npm, delete_friend, friend_list_json, friend_list, next_mahasiswa_list, \
    model_to_dict, csui_helper
from .models import Friend
from django.views.decorators.csrf import csrf_exempt
import json


##Create your tests here.
class Lab7UnitTest(TestCase):
     def test_lab_7_url_is_exist(self):
         response = Client().get('/lab-7/')
         self.assertEqual(response.status_code, 200)

     def test_lab_7_using_index_func(self):
         found = resolve('/lab-7/')
         self.assertEqual(found.func, index)

     def test_lab_7_friend_list_url_is_exist(self):
         response = Client().get('/lab-7/get-friend-list/')
         self.assertEqual(response.status_code, 200)

     def test_lab_7_friend_list_url_using_friend_list_func(self):
         found = resolve('/lab-7/get-friend-list/')
         self.assertEqual(found.func, friend_list)

     def test_model_to_dict_passed(self):
         new_activity = Friend.objects.create(friend_name='Wikans', npm='1606884413')
         data = model_to_dict(new_activity)
         self.assertIn('Wikans', data)
         self.assertIn('1606884413', data)

     def test_next_mahasiswa_list(self):
         response = Client().get('/lab-7/next-mahasiswa-list')
         self.assertEqual(response.status_code, 301)
         self.assertRedirects(response, '/lab-7/next-mahasiswa-list/', 301, 302)

     def test_prev_mahasiswa_list(self):
         response = Client().get('/lab-7/prev-mahasiswa-list')
         self.assertEqual(response.status_code, 301)
         self.assertRedirects(response, '/lab-7/prev-mahasiswa-list/', 301, 302)

     def test_all_POST_function(self):
         response_addFriend = Client().post('/lab-7/add-friend/',
                                            {'name': 'Wikans', 'npm': '1606884413'})
         response_deleteFriend = Client().post('/lab-7/delete-friend/', {'name': 'Wikans', 'npm': '1606884413'})
         response_validateNpm = Client().post('/lab-7/validate-npm/', {'npm': '1606884413'})
         response = Client().get('/lab-7/get-friend-list-json/')
